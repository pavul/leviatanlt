### IllusionTS
###### Typescript 2D game library

## Before Start
1. you need to have npm and node installed in your computer ( verify you have correct versions )
2. then you can execute command ```npm install``` this will install all devDependencies and dependencies needed (including typescript)
3. download or clone this repository from master branch
4. open repo or add it to your workspace in visual studio code ( you need this installed also )

## To Start the game
- execute ```npm run build``` command if you are in linux/mac or ```npm run build.win``` if you are in windows after that execute:
- ```npm run start``` this will start an http-server where game is hosted
- enter url provided for ```npm run start``` command (http://localhost:8080/game.html)
- (optional) ```npm run reset``` will build and start the game but only suppoered in unix OS for now


## Branches Content
- **master** this contains the game demo.


## Youtube Channel
- Here i show how to use the library, installation, level, sprite and some game examples, here you have a list of videos: https://www.youtube.com/watch?v=txcBRog8BhM&list=PLHzmvRibTrMdAaW02Yg3kvsTLrTbjwTW5&index=1

## Notes 
- at this moment i have content only in spanish, will be creating the same content but in english soon, stay tuned! 
