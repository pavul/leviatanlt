
import { Point } from "../lib/graphic/Point.js";
import { LineShape } from "../lib/graphic/shape/LineShape.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { MathUtil } from "../lib/util/MathUtil.js";
import { Timer } from "../lib/time/Timer.js";
import { Camera } from "../lib/camera/Camera.js";
import { Renderable } from "../lib/ntfc/Renderable.js";


/**
 * this is ahelper class that will be used to create a field
 * of star scrolling from right to left for some levels
 */
export class StarField implements Updatable, Renderable {

    stars: LineShape[];
    moveTimer: Timer;
    camera: Camera;
    multiColor: boolean;

    constructor(camera: Camera, byColors: boolean = false) {
        this.stars = [];
        for (let i: number = 0; i < 10; i++) {
            const p = new Point(MathUtil.getRandomRangeInt(1, 320),
                MathUtil.getRandomRangeInt(1, 160));

            this.stars[i] = new LineShape(p, { x: p.x + 1, y: p.y });

            this.stars[i].spdX = MathUtil.getRandomRangeInt(1, 100);

            this.multiColor = byColors;
            if (byColors) {
                this.stars[i].strokeColor = this.getRandomColor();
            }
        }

        this.camera = camera;

    }
    render(ctx: CanvasRenderingContext2D): void {

        for (const i in this.stars) {
            this.stars[i].render(ctx);
        }
    }


    update(delta: number, args?: any[] | undefined): void {
        for (const i in this.stars) {

            this.stars[i].points[0].x -= this.stars[i].spdX * delta;
            this.stars[i].points[1].x -= this.stars[i].spdX * delta;

            // this.stars[i].moveX(-(  this.stars[i].spdX * delta ) );

            if (this.stars[i].getX() < this.camera.viewX - 10) {

                const p = new Point(this.camera.viewX + this.camera.viewWidth + 50,
                    MathUtil.getRandomRangeInt(1, 160));

                this.stars[i].points[0].x = p.x;
                this.stars[i].points[0].y = p.y;

                this.stars[i].points[1].x = p.x + 1;
                this.stars[i].points[1].y = p.y;

                this.stars[i].spdX = MathUtil.getRandomRangeInt(1, 100);
                if (this.multiColor) {
                    this.stars[i].strokeColor = this.getRandomColor();
                }
            }
        }

    }


    getRandomColor() {
        let color = "#FFF";

        const rColor = MathUtil.choose([1, 2, 3, 4, 5, 6, 7, 8])
        switch (rColor) {
            case 1:
                color = "#7C7C7C";
                break;
            case 2:color = "#FC8D28";
                break;
            case 3:color = "#A8E048" ;
                break;
            case 4:color = "#7050A8";
                break;
            case 5:color = "#3465A4";
                break;
            case 6:color = "#FFCE00";
                break;
            case 7:color = "#C00000";
                break;
            case 8:color = "#FFFFFF";
                break;

        }
        return color;
    }

}