import { BaseLevel } from '../lib/level/BaseLevel.js';
import { GameState } from '../lib/manager/GameState.js';
import { Initiable } from '../lib/ntfc/Initiable.js';
import { Config } from '../cfg/Config.js';
import { LevelInitializer } from './LevelInitializer.js';
import { AnimationLoop } from '../lib/graphic/AnimationLoop.js';
import { Ship } from '../sprites/Ship.js';
import { SpriteUtil } from '../lib/util/SpriteUtil.js';
import { BaseSprite } from '../sprites/BaseSprite.js';
import { Bullet } from '../sprites/Bullet.js';
import { Missil } from '../sprites/Missil.js';
import { HUDSprite } from '../lib/graphic/HUDSprite.js';
import { Laser } from '../sprites/Laser.js';
// import { CollisionUtil } from '../lib/util/CollisionUtil.js';
import { BaseEnemy } from '../sprites/BaseEnemy.js';
import { GameData } from '../lib/manager/GameData.js';
import { EnemyState } from '../sprites/EnemyState.js';
import { StarField } from '../misc/StarField.js';
import { GameManager } from '../lib/manager/GameManager.js';
import { AudioManager } from '../lib/audio/AudioManager.js';
import { Boss1 } from '../sprites/Boss1.js';
import { GraphicManager } from '../lib/graphic/GraphicManager.js';


export class Level1 extends BaseLevel implements Initiable
{

    ship:Ship;

    powerups:BaseSprite[];
    enemyMap:Map<string,BaseEnemy>;

    gameData:GameData;

    starField:StarField;
    
    boss1:Boss1;

    //used to load all images that can be used to create sprites or
    //set new animations for sprites 
    imageMap: Map<string, HTMLImageElement>;

    constructor()
    {
        //setting level width and height
        // width 5120
        super( 320 * 20, Config.viewHieght, Config.viewWidth,  Config.viewHieght );

        this.imageMap = new Map<string, HTMLImageElement>();

        this.init(); //init can be also here instead GAMESTATE.LOADING
    }

    async init(): Promise<void> 
    {
        GameManager.getInstance().setFontSize(8);

        await this.loadSounds();
        await this.loadImages();

        const initializer = new LevelInitializer();
        
        //geting all the sprites generated from the atlas
        this.spriteMap = initializer.generateSprites( GraphicManager.getImage("shipAtlas"));
        
        //set position and prevent animation of the ship
        this.ship = <Ship>this.spriteMap.get("ship");
        this.ship.explotionImgMeasures = GraphicManager.getImage("shipAtlas").getImageData("ship_explosion").imageMeasures;
        this.ship.setPosition(20,100);
        this.ship.currentFrame = 1;
        this.ship.animationLoop = AnimationLoop.NONE;

        this.ship.camera = this.camera;


        this.ship.bullets = //<Bullet[]>[this.spriteMap.get("ship_bullet")]; 
            Array.from( SpriteUtil.getCopies<Bullet>(<Bullet>this.spriteMap.get("ship_bullet"), 10, Bullet, "bullet" ).values())  

        this.ship.bullets.forEach(b=>{b.visible=false;b.setPosition(0,-100)})

        this.ship.missils = [ <Missil>this.spriteMap.get("ship_missile0"), <Missil>this.spriteMap.get("ship_missile1") ]
        this.ship.missils[0].explosionImgMeasure = GraphicManager.getImage("shipAtlas").imageData.get("ship_mis_exp_down").imageMeasures;
        this.ship.missils[1].explosionImgMeasure = GraphicManager.getImage("shipAtlas").imageData.get("ship_mis_exp_up").imageMeasures;


        this.ship.lasers = 
            Array.from( SpriteUtil.getCopies<Laser>(<Laser>this.spriteMap.get("ship_laser"), 3, Laser, "laser" ).values())  
        this.ship.lasers.forEach(b=>{b.visible=false;b.setPosition(0,-100)})

        this.ship.drone = <BaseSprite>this.spriteMap.get("ship_drone");
        this.ship.shield = <BaseSprite>this.spriteMap.get("ship_shield");
        this.ship.flame = <BaseSprite>this.spriteMap.get("ship_flame");
        this.ship.flame.setAnimationFrames(1,2);

        //setting drone position
        this.ship.drone.setPosition( ( this.ship.getX()+(this.ship.w/2) ) - this.ship.drone.w/2 , 
        this.ship.getY()+(this.ship.h/2) - this.ship.drone.h/2 )

        this.ship.powerBarHud = <HUDSprite>this.spriteMap.get("hud_bar");

        this.powerups = initializer.generatePowerUps( GraphicManager.getImage("shipAtlas")) ;


        //generate the enmies and bullets below and set the 
        this.enemyMap = initializer.generateEnemies( GraphicManager.getImage("enemyAtlas") );
        BaseEnemy.powerups = this.powerups;//setting powerups


         this.boss1 = initializer.generateBoss1( GraphicManager.getImage("boss1Atlas") );
        this.boss1.bullets = 
            SpriteUtil.getSpriteGroupByLabel<BaseEnemy>("ene_bullet",
            Array.from( this.enemyMap.entries(), ([, value]) => value ));
        //put the enemies in random position 


        //create star field for the scrolling background
        this.starField = new StarField(this.camera);


        //playing bg music      
        AudioManager.play("bgmusic1");


        //@for testing
        // this.ship.powerups[1]=true;
        // this.ship.powerups[3]=true;


        this.gameState = GameState.PLAYING;
    }


    update(delta:number)
    {

        switch( this.gameState )
        {
            case GameState.LOADING:
                //  this.init();
            break;
            case GameState.PLAYING:

            //moving view to right by 1
            if( this.camera.viewX >= (this.levelWidth-this.camera.viewWidth) && !AudioManager.isPlaying( "enemymusic" ) )
            {
                AudioManager.stopAll();
                AudioManager.play("enemymusic")

            }
            else if( this.camera.viewX + this.camera.viewWidth < this.levelWidth )
            {
                const camSpd:number = 1;
                this.camera.moveX(camSpd);
                this.ship.moveX(camSpd);//ship needs to follow the view at same spd
                
                if(this.ship.powerups[4] && this.ship.drone.visible)
                {this.ship.drone.moveX(camSpd)}
                //check for shield, flame and drone
                // console.log(`camx: ${this.camera.x} - camVX: ${this.camera.viewX}`)  
            }

            this.starField.update(delta);

            


            //ENEMY movement
            for(const enemy of this.enemyMap.values())
            {
                enemy.update(delta, [this.ship, this.camera]);
            } 

            // Array.from(map.entries(), ([, value]) => value);

            //getting the group of enemies ( not bullets ) to check collisions between them
            // const enemyGroup = 
            //     SpriteUtil.getSpriteGroupByLabel("enemy", Array.from( this.enemyMap.entries(), ([, value]) => value ) )

                const enemyGroup = 
                    SpriteUtil.getSpritesInsideView<BaseEnemy>(Array.from( this.enemyMap.entries(), ([, value]) => value ), this.camera);

            

            if( this.camera.flashEffect.isActive)
            {
                this.camera.flashEffect.update( delta );

                for( const id in enemyGroup )
                {
                    const ene = enemyGroup[id];
                    if( ene.label.includes("ene_bullet"))
                    {
                        ene.state = EnemyState.DISABLED;
                    }
                    else
                    {
                        ene.state = EnemyState.DESTROYED;
                    }
                }
            }



            //update bullets 
            //console.log(`B length: ${this.ship.bullets.length}`)
            for( let b of this.ship.bullets )
            { 
                b.update(delta, [enemyGroup]) }

            //update missiles
            for( let m of this.ship.missils )
            { m.update(delta, [enemyGroup]); }

            //update lasers
            for( let l of this.ship.lasers )
            { l.update(delta, [enemyGroup]); }

            this.boss1.update( delta, [this.ship, this.camera] );

            this.ship.update(delta, [enemyGroup, this.powerups]);

                // this.changeLevelTask.process( ()=>
                // {
                //     GameManager.getInstance().loadLevel( new SampleLevel() );
                // });
            

                

            break;

            case GameState.PAUSED:
                //poll and get gamepadinput of the ship if paused
                //to unpause the game
                this.ship.gpad.poll( navigator.getGamepads() )
                this.ship.getGamepadInput();
            break;    
        }

    }


    render(ctx:CanvasRenderingContext2D)
    {

        switch( this.gameState )
        {
            case GameState.LOADING:
                break;
            case GameState.PLAYING:

            ctx.save();
            
            ctx.translate(this.camera.x, this.camera.y);


                // set black background color and fill canvas with it
                //but only on camera view 
                ctx.fillStyle = "#000";
                ctx.fillRect( this.camera.viewX, this.camera.viewY, this.camera.viewWidth, this.camera.viewHeight );

                //show starfield
                this.starField.render(ctx);

                //set white color and print hello word in screen at 20, 20
                // ctx.fillStyle = "#FFF";
                // ctx.fillText( "Hello World!" ,20,20);
                this.ship.render( ctx );
                
                ctx.strokeStyle = "#0F0";
                // const col = this.ship.colliders.get("col");
                // ctx.strokeRect( col.getX(), col.getY(), col.w, col.h );

                for(let i in this.powerups)
                {
                    this.powerups[i].render(ctx);
                }

                //rendering enemies
                for( const enemy of this.enemyMap.values())
                {
                    enemy.render(ctx);

                    // if(enemy.label.includes("ene_bullet"))
                    // {
                    //     ctx.strokeStyle = "#F00";
                    //     ctx.strokeRect( enemy.getX(), enemy.getY(), enemy.w, enemy.h );
                    // }

                }

                this.boss1.render(ctx);
               

            //to show flash effect if is activated
            this.camera.flashEffect.render(ctx);

            ctx.restore()



                break;
            
            case GameState.PAUSED:
                //show game paused menu

                ctx.save();
            
                ctx.translate(this.camera.x, this.camera.y);

                ctx.fillStyle = "#000";
                ctx.fillRect( this.camera.viewX, this.camera.viewY, this.camera.viewWidth, this.camera.viewHeight );

                ctx.fillStyle = "#FFF";
                ctx.fillText("Game Paused", this.camera.viewX+this.camera.viewWidth/2 - 50,this.camera.viewY + this.camera.viewHeight/2 )
            
                ctx.fillText(`Lives x ${GameData.getData("lives")}`, this.camera.viewX+20,this.camera.viewY + 20 )
                ctx.fillText(`Score: ${GameData.getData("score")}`, this.camera.viewX+160,this.camera.viewY + 20 )
            

                ctx.restore();
                break;    
        }
       
    }

    /**
     * this function load the images before they can be used
     */
    async loadImages(): Promise<void> {

    }

    async loadSounds(): Promise<void> 
    {
        

    }

    loadData(): void {
    }


    keyUp(event: KeyboardEvent): void {
        
        if(event.key === "Enter")
        {   
            AudioManager.play("pausesfx")
            switch(this.gameState)
            {
                case GameState.PAUSED: 
                this.gameState = GameState.PLAYING; 
                AudioManager.unmute();

                //this is to unpause the game in case the controller has disconnected
                this.ship.gpad.gamePad=undefined;
                break;
                case GameState.PLAYING:
                     this.gameState = GameState.PAUSED;
                     AudioManager.mute();
                break;
            }

        }
        
        
        //for listening input events for gamepad if connected
        if( this.gameState === GameState.PLAYING)
            this.ship.getInput( event.key ,"keyUp" );

    }

    keyDown(event: KeyboardEvent): void {
        // console.log("key down")
        if( this.gameState === GameState.PLAYING)
        this.ship.getInput( event.key , "keyDown" );
    }


    // pauseGame(pause:boolean)
    // {
    //     if(pause)
    //     {
    //         this.gameState = GameState.PAUSED;
            
    //     }
    //     else
    //     {
    //         this.gameState = GameState.PLAYING;
    //         AudioManager.play("pausesfx", false)
    //     }

    // }


}