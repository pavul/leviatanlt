import { Image } from "../lib/graphic/Image.js";
import { HUDDisplayType } from "../lib/graphic/HUDDisplayType.js";
import { HUDSprite } from "../lib/graphic/HUDSprite.js";
import { Sprite } from "../lib/graphic/Sprite.js";
import { MathUtil } from "../lib/util/MathUtil.js";
import { BaseEnemy } from "../sprites/BaseEnemy.js";
import { BaseSprite } from "../sprites/BaseSprite.js";
import { Boss1 } from "../sprites/Boss1.js";
import { Bullet } from "../sprites/Bullet.js";
import { EnemyType } from "../sprites/EnemyType.js";
import { Laser } from "../sprites/Laser.js";
import { Missil } from "../sprites/Missil.js";
import { Ship } from "../sprites/Ship.js";


/**
 * this will create the ship, missiles, bullets
 * enemies, bosses, powerups, etc in one place
 * to not DIY on each level
 */
export class LevelInitializer
{
    readonly MAX_POWERUPS:number=10;

    powerups:BaseSprite[]=undefined;

    constructor(){}

    generateSprites(atlas : Image)
    {

        // const sprites:BaseSprite[]=[];

        const spriteMap:Map<string,any> = new Map();
        // for (const key of myMap.keys()) {
        //     const value = myMap.get(key);
        //     console.log(`La clave es ${key} y el valor es ${JSON.stringify(value)}`);
        //   }
        //   for (const value of myMap.values()) {
        //     console.log(`El valor es ${JSON.stringify(value)}`);
        //   }

        for( const [key, value] of atlas.imageData.entries() )
        {
            // console.log(`key: ${key} val: ${value}`)
            const val = atlas.imageData.get(key).imageMeasures;
            let spr:any = undefined;
            // console.log(val)
   
            if( key === "ship" )
                spr = new Ship( atlas.image, val);
            else if( key === "ship_bullet" )
            {
                spr = new Bullet( atlas.image, val);
            }
            else if( key === "ship_laser" )
            {
                spr = new Laser( atlas.image, val);
            }
            else if( key === "ship_missile" )
            {
                // spr = new Missil( atlas.image, val);
                spriteMap.set(key+"0", new Missil( atlas.image, val));
                spriteMap.set(key+"1", new Missil( atlas.image, val));
                
                spriteMap.get(key+"0").visible= false;
                spriteMap.get(key+"1").visible= false;

                spriteMap.get(key+"1").missileType = 1;
                continue;
            }
            else if(key === "hud_bar")
            {
                spr = new HUDSprite( atlas.image, val, HUDDisplayType.IMAGE)
            }
            else if( key.includes( "ship_mis_exp") )
            {
                //we won't make missil explosion sprites, will use the missiles 
                //we will just change the animation
                continue;
            }
            else
                spr = new BaseSprite( atlas.image, val);
 
            spr.label = key;
            spriteMap.set(key, spr);
        }
        return spriteMap;
    }

    /**
     * will generate the power ups that raise the power gauge
     * @param atlas 
     */
    generatePowerUps(atlas : Image)
    {
        if(this.powerups === undefined)this.powerups=[];

        for( let idx=0; idx < this.MAX_POWERUPS; idx++)
        {
            this.powerups[idx] = new BaseSprite( atlas.image, atlas.imageData.get("powerup").imageMeasures );
            this.powerups[idx].visible=false;
            this.powerups[idx].setPosition(-100,-100);//move out of view

            // const rX = MathUtil.getRandomRangeInt(160,310);
            // const rY = MathUtil.getRandomRangeInt(10,170);
            // this.powerups[idx].setPosition(rX,rY);

        }
        return this.powerups;
    }


    generateEnemies(atlas : Image)
    {
        const spriteMap:Map<string,any> = new Map();
        
        
        for( let id:number = 0; id < 30 ;id++)
        {
            const spr1:BaseEnemy = new BaseEnemy(atlas.image, atlas.getImageData("ene1").imageMeasures, EnemyType.ENEMY_1, atlas.imageData );
            spr1.label="enemy";

            const spr2:BaseEnemy = new BaseEnemy(atlas.image, atlas.getImageData("ene2").imageMeasures, EnemyType.ENEMY_2, atlas.imageData )
            spr2.label="enemy";

            const spr3:BaseEnemy = new BaseEnemy(atlas.image, atlas.getImageData("ene3").imageMeasures, EnemyType.ENEMY_3, atlas.imageData )
            spr3.label="enemy";

            const spr4:BaseEnemy = new BaseEnemy(atlas.image, atlas.getImageData("ene4").imageMeasures, EnemyType.ENEMY_4, atlas.imageData )
            spr4.label="enemy";

            spriteMap.set("enemy1_"+id, spr1 );
            spriteMap.set("enemy2_"+id, spr2 );
            spriteMap.set("enemy3_"+id, spr3 );
            spriteMap.set("enemy4_"+id, spr4 );
        
            const bullet = new BaseEnemy(atlas.image, atlas.getImageData("enebullet").imageMeasures, EnemyType.BULLET, atlas.imageData )
            const sqrBullet = new BaseEnemy(atlas.image, atlas.getImageData("enebullet2").imageMeasures, EnemyType.SQR_BULLET, atlas.imageData )
        
            BaseEnemy.eneBullets.push(bullet);
            BaseEnemy.eneSquaredBullets.push(sqrBullet);

            bullet.visible=false; bullet.enable=false;
            bullet.label="ene_bullet";
            sqrBullet.visible=false; sqrBullet.enable=false;
            sqrBullet.label="ene_bullet,ene_sqrbullet";

            spriteMap.set("bullet_"+id,bullet );
            spriteMap.set("sqrBullet_"+id,sqrBullet);
        }

        return spriteMap;
    }


    generateBoss1(atlas : Image)
    {

        const boss1 = 
            new Boss1( atlas.image ,
                 atlas.getImageData("boss1").imageMeasures,
                  EnemyType.BOSS, 
                   atlas.imageData );

        return boss1;         
    }



}