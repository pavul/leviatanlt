
import { BaseLevel } from '../lib/level/BaseLevel.js';
import { Initiable } from '../lib/ntfc/Initiable.js';
import { AssetLoadable } from '../lib/ntfc/AssetLoadable.js';
import { AssetUtil } from "../lib/util/AssetUtil.js";
import { GameState } from '../lib/manager/GameState.js';
import { Timer } from "../lib/time/Timer.js";
import { GameManager } from '../lib/manager/GameManager.js';
import { SampleLevel } from './SampleLevel.js';
import { Config } from '../cfg/Config.js';
import { Level1 } from './Level1.js';
import { TitleLevel } from './TittleLevel.js';
import { Controller } from '../lib/input/Controller.js';


/**
 * this splash screen will wait until the user makes any event cause is
 * needed to enable the apies for gamepad and audio, so this is the way 
 * to do it so title screen can be 
 */
export class SplashScreenLevel extends BaseLevel
    implements AssetLoadable, Initiable
{


    changeLevelTask:Timer;
    alphaVal:number;

    textVisible:boolean;
    visibleTimer:Timer;

    // gpad:Controller;
    splashImage:HTMLImageElement;

    constructor()
    {
        super(Config.viewWidth,  Config.viewHieght);
        // this.init();
    }

    async init(): Promise<void>  {

        GameManager.getInstance().setFontSize(8);

        await this.loadImages();

        this.changeLevelTask = new Timer();
        this.changeLevelTask.setCounter(200);

        this.alphaVal = 0;

        this.textVisible=true;

        this.visibleTimer= new Timer();
        this.visibleTimer.setCounter(20);

        // this.gpad = new Controller(navigator.getGamepads()[0]);


        this.gameState = GameState.PLAYING;
    }

    async loadImages(): Promise<void> 
    {
        this.splashImage = await  AssetUtil.getImage("/assets/splash/BitlessGamesLogo.png").then(img=>img);
        // this.imageMap.set( "splashImage", splashImage );
    }

    loadSounds(): void {
    }

    loadData(): void {
    }


    update(delta:number)
    {

        switch( this.gameState )
        {
            case GameState.LOADING:
                 this.init();
            break;

            case GameState.GAMEPAD_CONNECTING:

            //this will check for gamepad events, 
            //if something is found then will show title screen
                
            case GameState.PLAYING:

                const pad = navigator.getGamepads()[0];
                if(pad !== null)
                {
                    console.log(`PAD goto level 1`)
                    this.gotoTitleLevel();
                }
            

                this.visibleTimer.process( ()=>{
                    this.textVisible = !this.textVisible;
                    this.visibleTimer.setCounter(20);
                });
                // this.changeLevelTask.process( ()=>
                // {
                //     GameManager.getInstance().loadLevel( new TitleLevel() );
                // });
            break;
        }

    }

    

    render( ctx:CanvasRenderingContext2D)
    {

        ctx.fillStyle = "#000";
        ctx.fillRect(0,0, this.levelWidth, this.levelHeight)

        ctx.save();
            if( this.alphaVal < 1)
                this.alphaVal+= 0.01;

                switch( this.gameState )
                {
                    case GameState.LOADING:
                    break;

                    case GameState.GAMEPAD_CONNECTING:
                    case GameState.PLAYING:
                        
                    

                    ctx.globalAlpha = this.alphaVal;
                    ctx.drawImage( this.splashImage, this.levelWidth/2 - this.splashImage.width/2, this.levelHeight/2 - this.splashImage.height/2 );
                 
                 
                 
                    if( this.alphaVal >= 1 && this.textVisible )
                    {
                        ctx.fillStyle = "#fff";
                        ctx.fillText("Press any key/button to continue...", 20, 140 );
                        // ctx.fillText("Press any key/button to continue...", 20, 160 );
                    }

                    ctx.restore();
                    break;
                }

    }

keyUp(event: KeyboardEvent): void {

    if( this.alphaVal >= 1)
        switch( this.gameState )
        {
            case GameState.PLAYING:
               this.gotoTitleLevel()
            break;
        }

}


gotoTitleLevel()
{
    GameManager.getInstance().loadLevel( new TitleLevel() );
}


}