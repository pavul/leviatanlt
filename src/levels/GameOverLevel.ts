import { AudioManager } from "../lib/audio/AudioManager.js";
import { GraphicManager } from "../lib/graphic/GraphicManager.js";
import { Controller } from "../lib/input/Controller.js";
import { BaseLevel } from "../lib/level/BaseLevel.js"
import { GameData } from "../lib/manager/GameData.js";
import { GameManager } from "../lib/manager/GameManager.js";
import { GameState } from "../lib/manager/GameState.js";
import { AssetLoadable } from "../lib/ntfc/AssetLoadable.js";
import { Initiable } from "../lib/ntfc/Initiable.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { Timer } from "../lib/time/Timer.js";
import { Level1 } from "./Level1.js";
import { HUDSprite } from "../lib/graphic/HUDSprite.js";
import { HUDDisplayType } from "../lib/graphic/HUDDisplayType.js";
import { Point } from "../lib/graphic/Point.js";
import { TitleLevel } from "./TittleLevel.js";



export class GameOverLevel extends BaseLevel
    implements Updatable, Initiable, AssetLoadable
{


    textVisible:boolean;
    visibleTimer:Timer;

    gpad:Controller;
    pressed:boolean;

    imageMap: Map<string, HTMLImageElement>;
    cursor:HUDSprite;
    selection:number;



    constructor()
    {
        super(320, 180);
        this.imageMap = new Map<string, HTMLImageElement>();
        this.init();
    }




    async loadImages(): Promise<void> 
    {

       

    }

    
    async loadSounds(): Promise<void> 
    {
        
    }

    loadData(): void
    {
    //    /**
    //     * the game data is created in this screen, once is created it should
    //     * remain availale on next levels
    //     */
    //     if( GameData.load() === undefined)
    //     {
    //         GameData.setData("lives",3);
    //         GameData.setData("continues",3);
    //         GameData.setData("score",0);
    //         GameData.setData("maxLevel",1);
    //     }
    }

    async init(): Promise<void> {
        
        // this.textVisible = true;
        // this.visibleTimer = new Timer(20);
        
        GameManager.getInstance().setFontSize(8);


        // this.loadData();

        // await this.loadSounds();

        // await this.loadImages();


        // AudioManager.play("bgmusictitle");

        this.gpad = new Controller(navigator.getGamepads()[0]);

        this.pressed=false;
        const shipImg = GraphicManager.getImage("shipAtlas");
        console.log(shipImg.getImageData("ship").imageMeasures)
        this.cursor = new HUDSprite( shipImg.image, shipImg.getImageData("ship").imageMeasures, HUDDisplayType.IMAGE )

        this.cursor.setPosition( 70, 90)

        this.selection = 1;

        this.gameState = GameState.PLAYING;
    }

    update(delta: number): void {
        
        switch( this.gameState )
        {
            case GameState.LOADING:
            break;
            case GameState.PLAYING:

            this.gpad.poll(navigator.getGamepads());

            // if(this.gpad.isButtonReleased(Controller.PS4_BTN_X))
            // {
            //     this.gotoLevel1()
            //     break;
            // }

            // // this.audioManager.isPlaying("bgmusic");
            //     this.visibleTimer.process(()=>{
            //         this.textVisible = !this.textVisible;
            //         this.visibleTimer.setCounter(20);
            //     })
            break;
        }

    }

    render(ctx: CanvasRenderingContext2D): void {
        switch( this.gameState )
        {

            case GameState.LOADING:
            break;
                case GameState.PLAYING:
                
                ctx. fillStyle ="#000";
                ctx.fillRect( 0,0, this.levelWidth, this.levelHeight);

                ctx. fillStyle ="#FFF";
                

            // ctx.drawImage(this.imageMap.get("titlescreen"), 0, 0 );

                ctx. fillStyle ="#FFF";
                ctx.fillText( "Game Over", 130, 70 );
                ctx.fillText( "Continue "+ GameData.getData("continues"), 130, 90 );
                ctx.fillText( "Quit", 130, 110 );
            

            
                if( this.selection===1)
                {this.cursor.render(ctx, 1, new Point( 100, 87))}
                else
                {this.cursor.render(ctx, 1, new Point( 100, 107))}
                

            break;
        }
    }


    keyUp(event: KeyboardEvent): void 
    {


        switch(event.key)
                {

                    case "w":
                    case "W":
                    case"ArrowUp":
                    this.selection = 1;
                    break;

                    case "s":
                    case "S":
                    case"ArrowDown":
                    this.selection = 2;
                    break;

                    case " "://space btn
                    case "j":
                    case "J":
                    case "z":
                    case "Z":
                       // to make sure whe only press once
                        if( !this.pressed )
                        {
                            this.pressed=true;

                            switch( this.selection )
                            {
                                case 1:
                                    AudioManager.play("startSfx");

                                    //continue to last level
                                    // decrease
                                    let continues= GameData.getData("continues", true);//true to return number istead string
                                    continues--;
                                    GameData.setData("continues", continues);
                                    GameData.setData("lives", 3);
                                    setTimeout(()=>{
                                        
                                        this.gotoLevel1()
                                    }, 2000)
                                break;
                                case 2:
                                    AudioManager.play("startSfx");
                                    setTimeout(()=>{
                                        GameManager.getInstance().loadLevel( new TitleLevel() )
                                    }, 2000)
                                break;        
                            }

                            
                        }
                    break;
                }
    }

    gotoLevel1()
    {
        AudioManager.stopAll();
        GameManager.getInstance().loadLevel( new Level1() )
    }

}