import { AudioManager } from "../lib/audio/AudioManager.js";
import { GraphicManager } from "../lib/graphic/GraphicManager.js";
import { Controller } from "../lib/input/Controller.js";
import { BaseLevel } from "../lib/level/BaseLevel.js"
import { GameData } from "../lib/manager/GameData.js";
import { GameManager } from "../lib/manager/GameManager.js";
import { GameState } from "../lib/manager/GameState.js";
import { AssetLoadable } from "../lib/ntfc/AssetLoadable.js";
import { Initiable } from "../lib/ntfc/Initiable.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { Timer } from "../lib/time/Timer.js";
import { AssetUtil } from "../lib/util/AssetUtil.js";
import { Level1 } from "./Level1.js";
import { Image } from "../lib/graphic/Image.js";



export class TitleLevel extends BaseLevel
    implements Updatable, Initiable, AssetLoadable
{


    textVisible:boolean;
    visibleTimer:Timer;

    gpad:Controller;
    pressed:boolean;

    imageMap: Map<string, HTMLImageElement>;


    constructor()
    {
        super(320, 180);
        this.imageMap = new Map<string, HTMLImageElement>();
        this.init();
    }




    async loadImages(): Promise<void> 
    {

        /**
         * like title level does not have a lot of logic is a good place to load the 
         * resources that can be used later 
         */
        GraphicManager.getInstance();//init all g manager properties

        if( !GraphicManager.loaded )
        {

            //LOADING IMAGES
            const shipImg= await  AssetUtil.getImage("/assets/atlas/shipatlas.png").then(img=>img);
            // this.imageMap.set( "shipAtlas", shipAtlas );
            const enemyImg = await  AssetUtil.getImage("/assets/atlas/eneatlas.png").then(img=>img);
            // this.imageMap.set( "enemyAtlas", enemyAtlas );
            const boss1Img = await  AssetUtil.getImage("/assets/atlas/boss1atlas.png").then(img=>img);
            // this.imageMap.set( "boss1Atlas", boss1Atlas );


            //LOADING CORRESPONDING JSON IMAGES
            const shipAtlasJson = await AssetUtil.makeAsyncRequest( "GET", "/assets/atlas/shipatlas.json", false );
            const enemyAtlasJson = await AssetUtil.makeAsyncRequest( "GET", "/assets/atlas/eneatlas.json", false );
            const boss1AtlasJson = await AssetUtil.makeAsyncRequest( "GET", "/assets/atlas/boss1atlas.json", false );

            /**
             * - shipAtlas contians a big image of all the animations related to the ship, 
             * ship flame, missiles, laser bullets, etc.
             * 
             * - enemyAtlas contains a big image of all animations related to the enmies, 
             * enemies, turrets, enemy bullets and lasers, asteroids , etc
             */
            //generate the atlas for ship and enemies
            const shipAtlas:Image =  new Image( shipImg, JSON.parse(shipAtlasJson) );
            const enemyAtlas:Image = new Image( enemyImg, JSON.parse(enemyAtlasJson) );
            const boss1Atlas:Image = new Image( boss1Img, JSON.parse(boss1AtlasJson) );

            GraphicManager.loadImage("shipAtlas", shipAtlas);
            GraphicManager.loadImage("enemyAtlas", enemyAtlas);
            GraphicManager.loadImage("boss1Atlas", boss1Atlas);

            GraphicManager.loaded=true;
        }


        /**
         * this title screen is only for this level, there is no point to load this into Graphic Manager
         */
        let titlescreen =  await AssetUtil.getImage("/assets/screen/titlescreen.png").then(img=>img);
        this.imageMap.set( "titlescreen", titlescreen );

    }

    
    async loadSounds(): Promise<void> 
    {
        AudioManager.getInstance();//setup all internal values 


        if( !AudioManager.loaded )
        {
            const bgmusictitle = await AssetUtil.getAudioBuffer( "/assets/sound/Juhani_Junkala_Chiptune_Adventures.ogg" );
            const startSfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_stargame" );
            
            AudioManager.addSound( "bgmusictitle", bgmusictitle, true );
            AudioManager.addSound( "startSfx", startSfx);  
    
    
            const bgmusic1 = await AssetUtil.getAudioBuffer( "/assets/sound/Juhani_Junkala_Chiptune_Adventures_stg1.ogg" );
            
            const enemymusic = await AssetUtil.getAudioBuffer( "/assets/sound/CommonFight.ogg" );
            


            const lasersfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_laser" );
            const shootsfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_shoot" );
            const shipexpsfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_shipexplosion" );
            const pausesfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_pause" );
            const shieldcolsfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_shieldcollision" );
            const eneexplsfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_enexplosion" );
            const misexplsfx = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_missilexplotion" );
            const getpower = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_movecursor" );
            const selectpower = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_powerup" );
            const missionComplete = await AssetUtil.getAudioBuffer( "/assets/sound/sfx/snd_missionaccomplished" );

    
            AudioManager.setMusicVolume(0.3);
            AudioManager.setSfxVolume(0.1);
    
            AudioManager.addSound( "bgmusic1", bgmusic1, true );
            AudioManager.addSound( "enemymusic", enemymusic, true );

            AudioManager.addSound( "lasersfx",lasersfx );  
            AudioManager.addSound( "shootsfx",shootsfx );
            AudioManager.addSound( "shipexpsfx",shipexpsfx );
            AudioManager.addSound( "pausesfx",pausesfx );
            AudioManager.addSound( "shieldcolsfx",shieldcolsfx );
            AudioManager.addSound( "eneexplsfx",eneexplsfx );
            AudioManager.addSound( "misexplsfx",misexplsfx );
            AudioManager.addSound( "misexplsfx",misexplsfx );
            AudioManager.addSound( "getpower",getpower );
            AudioManager.addSound( "selectpower",selectpower );
            AudioManager.addSound( "missionComplete",missionComplete );

            AudioManager.loaded=true;

        }

        
        
    }

    loadData(): void
    {
       /**
        * the game data is created in this screen, once is created it should
        * remain availale on next levels
        */
        // if( GameData.load() === undefined)
        // {
            //set the data always we are in title:
            // if first time or if continues were consumed
            GameData.setData("lives",3);
            GameData.setData("continues",3);
            GameData.setData("score",0);
            GameData.setData("maxLevel",1);
        // }
    }

    async init(): Promise<void> {
        
        this.textVisible = true;
        this.visibleTimer = new Timer(20);
        
        GameManager.getInstance().setFontSize(8);


        this.loadData();

        await this.loadSounds();

        await this.loadImages();


        AudioManager.play("bgmusictitle");

        this.gpad = new Controller(navigator.getGamepads()[0]);

        this.pressed=false;

        this.gameState = GameState.PLAYING;
    }

    update(delta: number): void {
        
        switch( this.gameState )
        {
            case GameState.LOADING:
            break;
            case GameState.PLAYING:

            this.gpad.poll(navigator.getGamepads());

            if(this.gpad.isButtonReleased(Controller.PS4_BTN_X))
            {
                this.gotoLevel1()
                break;
            }

            // this.audioManager.isPlaying("bgmusic");
                this.visibleTimer.process(()=>{
                    this.textVisible = !this.textVisible;
                    this.visibleTimer.setCounter(20);
                })
            break;
        }

    }

    render(ctx: CanvasRenderingContext2D): void {
        switch( this.gameState )
        {

            case GameState.LOADING:
            break;
                case GameState.PLAYING:
                    // ctx. fillStyle ="#000";
            ctx.drawImage(this.imageMap.get("titlescreen"), 0, 0 );

            if( this.textVisible )
            {
                ctx. fillStyle ="#FFF";
                ctx.fillText( "Press Z/J to Start Game", 60, 100 );
            }
            
            break;
        }
    }


    keyUp(event: KeyboardEvent): void 
    {
        switch(event.key)
                {
                    //to shoot
                    case " "://space btn
                    case "j":
                    case "J":
                    case "z":
                    case "Z":
                        // to make sure whe only press once
                        if(!this.pressed)
                        {
                            this.pressed=true;
                            AudioManager.stopAll();
                            AudioManager.play("startSfx");
                            setTimeout(()=>{
                                this.gotoLevel1()
                            }, 2000)
                        }
                    break;
                }
    }

    gotoLevel1()
    {
        AudioManager.stopAll();
        GameManager.getInstance().loadLevel( new Level1() )
    }

}