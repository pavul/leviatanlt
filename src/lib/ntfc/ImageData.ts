import { ImageMeasures } from "../graphic/ImageMeasures.js";



export interface ImageData
{
    name:string;
    label:string;
    copies:number;
    imageMeasures:ImageMeasures;
}