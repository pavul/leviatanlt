import { Camera } from "./Camera.js";


/**
 * this is a base class to create different Camera Effects
 */
export abstract class CameraFX
{

    isActive:boolean;
    time:number;
    // duration:number;
    // alpha:number;

    camera:Camera;

    constructor(cam:Camera)
    {
        this.camera =cam;
        this.isActive=false;
        this.time=0;
        // this.duration=0;
        // this.alpha=1;
    }


}