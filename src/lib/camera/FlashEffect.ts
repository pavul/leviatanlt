
import { Renderable } from "../ntfc/Renderable";
import { Updatable } from "../ntfc/Updatable";
import { Camera } from "./Camera.js";
import { CameraFX } from "./CameraFX.js";



export class FlashEffect extends CameraFX
implements Renderable, Updatable
{

    // isFlashing:boolean;
    // time:number; //this will count how much time has elapsed
    // // prevPosition:Point;
    // camera:Camera;
    alpha:number;
    color:string;
    duration:number;//duration in seconds
    

    constructor(cam:Camera)
    {
        super(cam);
        this.camera=cam;
        this.duration = 0.3;
        this.color="#FFF"; //white by default

    }

     /**
     * call this method to execute shake, but method for flashing (updateFlash)
     * should be called in Level.update()
     * @param makeShake 
     */
      activate( active:boolean )
      {
          this.isActive = active;
          this.alpha = 1;
      }


      /**
       * args[0] can be new duration by default 0.3 second
       * args[1] can be the color of the flash by default white
       * @param deltaTime 
       * @param args 
       */
      update(deltaTime:number, args?: any[])
      {
         const dur = args && args[0]? args[0]: this.duration;
         const col = args && args[1]? args[1]:this.color;
          
        if( this.isActive )
        {
            console.log(`delta ${deltaTime} - duration ${dur}`)
            this.time += ( deltaTime );

            if(this.time >= this.duration)
            {
                this.isActive = false;
                this.time = 0;
            }
            this.alpha = this.time/dur;
        }

      }

    /**
     * this will only draw rect at filled color while time flashing is lasting
     * @param ctx 
     */
      render(ctx:CanvasRenderingContext2D)
      {

        if( this.isActive )
        {
            ctx.save();
            ctx.globalAlpha = this.alpha;
            ctx.fillStyle = this.color;
            ctx.fillRect( this.camera.viewX , this.camera.viewY, this.camera.viewWidth, this.camera.viewHeight );
            ctx.restore();
        }
        
      }

}