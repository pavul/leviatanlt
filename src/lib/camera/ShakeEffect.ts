import { Point } from "../graphic/Point.js";
import { Updatable } from "../ntfc/Updatable.js";
import { MathUtil } from "../util/MathUtil.js";
import { Camera } from "./Camera.js";
import { CameraFX } from "./CameraFX.js";



export class ShakeEffect extends CameraFX
implements Updatable
{

    // isShaking:boolean;
    shakeStepCounter:number;
    // time:number;
    prevPosition:Point;
    // camera:Camera;
    

    constructor(cam:Camera)
    {
        super(cam)
        this.shakeStepCounter=0;
        // this.isShaking=false;
        // this.time=0;
        this.prevPosition = new Point(0,0);

    }

     /**
     * call this method to execute shake, but shake method should be called in Level.update()
     * @param makeShake 
     */
      activate( active:boolean )
      {
          this.isActive = active;
          this.prevPosition =  new Point(this.camera.x,this.camera.y);
      }
      /**
       * this method should be put in Level.update, this will execute camera shake with defined variables
       * @param amplitude how muy will move in pixels
       * @param duration number of steps will last by default 60
       */
      
      /**
       * this method should be put in Level.update, this will execute camera shake with defined variables.
       * @param deltaTime  incomming delta time from update method
       * @param args[0] amplitude how much the camera will shake in pixels
       * @param args[1] duration this should be in secords by default 1 second, this will be compared against deltaTime
       * @param args[2] frecuency how many times per step it will be shaken
       */
      update( deltaTime:number,args?:any[] )//needs duration
      {

        const amplitude = args && args[0]?args[0]:16;
        const duration = args && args[1]?args[1]:1;
        const frecuency = args && args[2]?args[2]:6;

          if( this.isActive )
          {
              this.shakeStepCounter++;
              this.time += (deltaTime * duration);
  
              this.camera.x =this.prevPosition.x; 
              this.camera.y =this.prevPosition.y; 
            //   console.log(" frecuency/60 :",frecuency/60)
              if(this.shakeStepCounter >= frecuency/60 )
              {
                  this.camera.x += MathUtil.getRandomRangeInt( -amplitude, amplitude );
                  this.camera.y += MathUtil.getRandomRangeInt( -amplitude, amplitude );
                  this.shakeStepCounter=0;
              }
      
              if(this.time >= duration)
              {
                  this.isActive=false;
                  this.time=0;
                  this.camera.x = this.prevPosition.x; 
                  this.camera.y = this.prevPosition.y;
                //   console.log("END of SHAKING")
              }
          }
  
      }
  

}