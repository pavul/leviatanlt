import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
import { BaseEnemy } from "./BaseEnemy.js";
import { EnemyType } from "./EnemyType.js";
import { ImageData } from "../lib/ntfc/ImageData.js"
import { Sprite } from "../lib/graphic/Sprite.js";
import { Collider } from "../lib/graphic/shape/Collider.js";
import { Camera } from "../lib/camera/Camera.js";
import { EnemyState } from "./EnemyState.js";
import { MathUtil } from "../lib/util/MathUtil.js";
import { Timer } from "../lib/time/Timer.js";
import { SpriteUtil } from "../lib/util/SpriteUtil.js";
import { Ship } from "./Ship.js";
import { CollisionUtil } from "../lib/util/CollisionUtil.js";
import { GraphicManager } from "../lib/graphic/GraphicManager.js";
import { AudioManager } from "../lib/audio/AudioManager.js";


export class Boss1 extends BaseEnemy
{

    flameUp:Sprite;
    flameDown:Sprite;
    shieldBars:Sprite[]=[];
    coreShield:Sprite;

    shieldBarsHp:number[]=[];
    coreShieldHp:number;
    coreHp:number;//for core collider


    //boss colliders, those are put on top of the 
    //boss sprite to collider to different parts of it
    upCollider:Collider;
    downCollider:Collider;
    coreCollider:Collider;
    
    bullets:BaseEnemy[];

   

    constructor(img: HTMLImageElement, imagMeasures: ImageMeasures, 
                enemyType: EnemyType, imgDatas: Map<string, ImageData>) 
    {
            
        super(img, imagMeasures, enemyType, imgDatas);
        // console.log(img)
    
        this.upCollider = new Collider(14, 12, 45, 27, this);
        this.downCollider = new Collider(14, 59, 45, 27, this);
        this.coreCollider = new Collider(22, 40, 14, 18, this);

        this.flameUp = new Sprite(img, imgDatas.get("boss1flameup").imageMeasures)
        this.flameDown = new Sprite(img, imgDatas.get("boss1flamedwn").imageMeasures)
        
        this.coreShield = new Sprite( img, imgDatas.get("coreshield").imageMeasures)

        for( let i:number = 0; i < 3 ;i++ )
        {
            this.shieldBars[i] = new Sprite( img, imgDatas.get("yellowBar").imageMeasures );
            this.shieldBarsHp[i] = 50;
        }

        this.coreShieldHp=100;
        this.coreHp=100;

        this.spdX=20;
        this.spdY=20;

        this.setPosition( (320*20) + 20 , 50 );
        // this.setPosition(300, 30);
        // this.enable=true;
        this.state=EnemyState.MOVE_LEFT;

        this.shootTimer = new Timer(100);

       
    }//

    update(delta: number, args?: any[]): void 
    {
        const ship = <Ship>args[0];
        const cam = <Camera>args[1];
        this.isWaiting(cam);


        if(!this.enable)return;

        //set boss sprites at position
        /**
         * movement of the boss
         */
        switch(this.state)
        {
            case EnemyState.MOVE_LEFT: 
                // console.log(` ${this.getX()} > ${cam.viewX+(cam.viewWidth-100)} `)
                if( this.getX() > cam.viewX+(cam.viewWidth-100) )
                    this.moveX(-(this.spdX*delta) );
                else   
                {
                    this.state = MathUtil.choose([ EnemyState.MOVE_UP, EnemyState.MOVE_DOWN]);
                    this.spdX=10;
                    this.spdY=10;
                }
                  
            break;
            case EnemyState.MOVE_UP:
                
                if( this.getY() > 30 )
                    this.moveY( -(this.spdY*delta) )
                else 
                    this.state = EnemyState.MOVE_DOWN;

            break;
            case EnemyState.MOVE_DOWN:
                if( this.getY()+this.h < 160 )
                    this.moveY( (this.spdY*delta) )
                else 
                    this.state = EnemyState.MOVE_UP;
            break;        

            case EnemyState.DESTROYED:
                if(this.animationEnd && this.visible)
                {
                    this.increaseScore(1000);
                    this.visible=false


                   ship.stageClear();
                   
                }
                break;
        }
    
        if(this.state === EnemyState.DESTROYED )return;

        // fix sprite positions related to boss
        this.flameUp.setPosition( this.getX()+77, this.getY()+16);
        this.flameDown.setPosition( this.getX()+77, this.getY()+70);
        this.coreShield.setPosition(this.getX()+22, this.getY()+44)
        this.shieldBars[0].setPosition(this.getX()+5, this.getY()+40);
        this.shieldBars[1].setPosition(this.getX()+10, this.getY()+40);
        this.shieldBars[2].setPosition(this.getX()+15, this.getY()+40);


        this.shootTimer.process( ()=>
        {

            let pattern = MathUtil.choose([1,2,3,4])
            const bArr=[];
            for( let i:number = 0; i < this.bullets.length ;i++)
            {
                if( !this.bullets[i].visible )
                {
                    this.bullets[i].visible=true;
                    this.bullets[i].enable=true;
                    
                    this.bullets[i].spdX=60;
                    this.bullets[i].spdY=60;
                    this.bullets[i].isInsideTimer.setCounter(20);
                    bArr.push( this.bullets[i] );
                    if( bArr.length === 4)break;
                }
            }

            const xx = ship.getX() + (this.w / 2);
            const yy = ship.getY() + (this.h / 2);

            // pattern=1;
            // console.log(`pattern ${pattern}`)
            switch( pattern )
            {
                case 1:
                if(bArr[0])
                {
                    bArr[0].state = EnemyState.MOVE_LEFT;
                    bArr[0].setPosition( this.getX()+19, this.getY()+17 )
                }
                if(bArr[1])
                {
                    bArr[1].state = EnemyState.MOVE_LEFT;
                    bArr[1].setPosition( this.getX()+19, this.getY()+81 )
                }
                if(bArr[2])
                {
                    bArr[2].state = EnemyState.MOVE_LEFT;
                    bArr[2].setPosition( this.getX()+2, this.getY()+37 )
                }
                if(bArr[3])
                {
                    bArr[3].state = EnemyState.MOVE_LEFT;
                    bArr[3].setPosition( this.getX()+2, this.getY()+62 )
                }

                    break;
                case 2:
                case 3:
                if(bArr[0])
                {
                    bArr[0].state = EnemyState.MOVE_LEFT;
                    bArr[0].setPosition( this.getX()+19, this.getY()+17 )
                }
                if(bArr[1])
                {
                    bArr[1].state = EnemyState.MOVE_LEFT;
                    bArr[1].setPosition( this.getX()+19, this.getY()+81 )
                }
                if(bArr[2])
                {
                    bArr[2].state = EnemyState.MOVE_ANY;
                    bArr[2].setPosition( (this.getX()+2) -bArr[2].w/2 , (this.getY()+37)-bArr[2].h/2 )
                    SpriteUtil.moveTo<BaseEnemy>( bArr[2], xx, yy, 90);
                    
                }
                if(bArr[3])
                {
                    console.log( `${ship.getX()} - ${ship.getY()}` )
                    console.log( `antes bullet 4:` )
                    console.log(bArr[3])
                    bArr[3].state = EnemyState.MOVE_ANY;
                    bArr[3].setPosition( (this.getX()+2) - bArr[3].w/2, (this.getY()+62) -bArr[3].h/2 )
                    SpriteUtil.moveTo<BaseEnemy>( bArr[3], xx, yy, 90 );
                    console.log( `bullet 4:` )
                    console.log(bArr[3])
                    
                }
                    
                
                    break;
                case 4:
                    break;
            }//suich


            this.shootTimer.setCounter( MathUtil.getRandomRangeInt(25,100) )
        });


        // collision between ship/bullets and boss
        const colUtil = CollisionUtil.getInstance();

        if( ship.state === ship.STATE_NORMAL )
        if( colUtil.spriteRectangleCollision( ship.colliders.get("col"), this.coreCollider  ) ||
            colUtil.spriteRectangleCollision( ship.colliders.get("col"), this.upCollider  ) ||
            colUtil.spriteRectangleCollision( ship.colliders.get("col"), this.downCollider ) || 
            colUtil.spriteRectangleCollision( ship.colliders.get("col"), this.coreCollider  ))
        {
            ship.destroy();
        }


        //boss1 ship bullet collision 
        for( const i in ship.bullets )
        {
            const b = ship.bullets[i];
            if(b.visible)
            {
                this.checkCollision(b, colUtil, this.bulletDmg);
            } 
        }//bnullet collisions

        for( const i in ship.lasers )
        {
            const b = ship.lasers[i];
            if(b.visible)
            {
                this.checkCollision(b, colUtil, this.laserDmg);
            }
        }

        //collision with missiles
        if( ship.missils[0].visible && !ship.missils[0].exploded)
        {
            this.checkCollision( ship.missils[0] , colUtil, this.bulletDmg);
        }
       
        if( ship.missils[1].visible && !ship.missils[1].exploded)
        {
            this.checkCollision( ship.missils[1] , colUtil, this.bulletDmg);
        }

        
    }//update

    render(ctx: CanvasRenderingContext2D): void {
        
        super.render(ctx);

        if( this.state != EnemyState.DESTROYED && this.visible )
        {
            this.flameDown.render(ctx);
            this.flameUp.render(ctx);
            this.coreShield.render(ctx);
        }
        
        for( const i in this.shieldBars)
        {
            this.shieldBars[i].render(ctx);
        }


        //SHIP COLLIDERS FOR TESTING
        // ctx.strokeStyle = "#F00";
        // ctx.strokeRect( this.coreCollider.getX(), this.coreCollider.getY(), this.coreCollider.w, this.coreCollider.h )
        // ctx.strokeRect( this.upCollider.getX(), this.upCollider.getY(), this.upCollider.w, this.upCollider.h )
        // ctx.strokeRect( this.downCollider.getX(), this.downCollider.getY(), this.downCollider.w, this.downCollider.h )

    }


    /**
     * checking coliisions with any kuind of bullet can be
     * bullet , laser or missile
     * @param b 
     * @param colUtil 
     */
    checkCollision(b:any, colUtil:any, dmg:number)
    {
        
        if(b.visible)
        {

            //collision with up/down colliders
            if( colUtil.spriteRectangleCollision( b, this.upCollider  ) ||
                colUtil.spriteRectangleCollision( b, this.downCollider  ) )
            {

                if( b.exploded !== undefined && !b.exploded )
                {
                    b.exploteMissil();
                }
                else{
                    b.visible=false;
                    b.enable=false;
                }

            }


            //collision with core shield
            if( colUtil.spriteRectangleCollision( b, this.coreShield ) && this.coreShield.visible )
            {
                if( b.exploded !== undefined && !b.exploded )
                {
                    b.exploteMissil();
                }
                else{
                    b.visible=false;
                    b.enable=false;
                }

                this.coreShieldHp -= dmg;
                if( this.coreShieldHp <= 0 )
                {this.coreShield.visible=false;}

            }
            else if( colUtil.spriteRectangleCollision( b, this.coreCollider ) )
            {//if there is no core shield we can hit the main core
                
                //decrease coreCollider
                if( b.exploded !== undefined && !b.exploded )
                {
                    b.exploteMissil();
                }
                else{
                    b.visible=false;
                    b.enable=false;
                }

                this.coreHp-=dmg;

                if( this.coreHp <= 0)
                {
                    this.coreHp = 0;
                    //enemy destroyed, change animation to explotion
                    this.state=EnemyState.IDDLE;
                    this.shootTimer.setCounter( 0 )
                    this.destroy()
                    // setTimeout()
                }

            }


            //collision with bars
            for( let i:number=0; i<3 ;i++)
            {
                const bar = this.shieldBars[i];

                if( bar.visible && colUtil.spriteRectangleCollision(b, bar) )
                {
                    if( b.exploded !== undefined && !b.exploded )
                    {
                        b.exploteMissil();
                    }
                    else{
                        b.visible=false;
                        b.enable=false;
                    }

                    this.shieldBarsHp[i]-= dmg;
                    if(this.shieldBarsHp[i] <= 0 )
                    {
                        bar.visible=false;
                    }
                }     
            
            }

        } 
    }



    destroy(): void {
        
        this.state = EnemyState.DESTROYED;

        AudioManager.stopAll();
        AudioManager.play("eneexplsfx");
        const exploImg = GraphicManager.getImage("enemyAtlas")

        const imgMsrs = exploImg.getImageData("bossenemyexplosion").imageMeasures;
        SpriteUtil.setAtSpriteCenter( this, {x:0,y:0, w:imgMsrs.w, h:imgMsrs.h} )
        this.setNewAnimation( exploImg.image, imgMsrs )
        
        this.animationStepLimit=6;

    }

}