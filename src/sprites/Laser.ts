import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { Timer } from "../lib/time/Timer.js";
import { CollisionUtil } from "../lib/util/CollisionUtil.js";
import { BaseSprite } from "./BaseSprite.js";
import { EnemyState } from "./EnemyState.js";



export class Laser extends BaseSprite implements Updatable
{
    insideTimer:Timer;
    
    constructor(image: HTMLImageElement, imgMeasures?:ImageMeasures)
    {
        super(image,imgMeasures);
        this.spdX=200;

        this.insideTimer= new Timer();
        this.insideTimer.setCounter(50);
    }

    update(delta: number, args?: any[]):void
    {
        
        // console.log("moving bullet")
        if(this.visible && this.enable)
        {
            if(this.visible && this.enable)
            {
                //go right
                this.moveX( this.spdX * delta );

                //check for any enemy collision
                //check collisions with enemies not bullets
                const enemies = args[0];
                for( const enemy of enemies)
                {
                    if(enemy.label.includes("ene_bullet"))continue;
                    
                if( enemy.visible && enemy.enable && enemy.state !== EnemyState.DESTROYED && enemy.state !== EnemyState.DISABLED )
                {
                    const isColliding = CollisionUtil.getInstance().spriteRectangleCollision(this, enemy)
                    if( isColliding )
                    {
                        if( enemy.label.includes("boss") )
                        {
                            //deal damage to boss
                        }
                        else//check for other kinds of enemies
                        {
                        //laser wont dissapear but enemy will be destroyed
                        // enemy.destroy();
                        enemy.state = EnemyState.DESTROYED;
                        }


                    }
                }
                }


                //check if out of view and disable/destroy
                //check if out of view and disable/destroy
                this.insideTimer.process(()=>{
                    //if outside the view, then disable it
                    if( !this.isInsideView(this.getX(), this.getY(), this.w, this.h ))
                    {
                        this.visible=false;
                        this.enable=false;
                    }
                    this.insideTimer.setCounter(50);
                });

            }
            
        }

        

        

    }

}