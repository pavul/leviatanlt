import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
import { Sprite } from "../lib/graphic/Sprite.js";
import { GameManager } from "../lib/manager/GameManager.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { CollisionUtil } from "../lib/util/CollisionUtil.js";


/**
 * common vars for all sprites of the game
 * 
 * enable is used for those sprites outside of the view, 
 * when the sprite is in the rendered part of the view
 * then enabled will be true and will start moving/attacking/etc.
 * 
 * 
 */
export class BaseSprite extends Sprite implements Updatable
{

    enable:boolean;

    constructor(image: HTMLImageElement, imgMeasures?:ImageMeasures)
    {
        super(image, imgMeasures);
        this.enable=false;
    }

    
    update(delta: number, args?: any[]): void {
    }


    /**
     * used to change the animation to explosion in case
     * enemies are destroyed, for the ship this will show game over screen
     */
    destroy()
    {
        this.visible=false;
        this.enable=false;
    }


    isInsideView(x:number, y:number, w:number, h:number)
    {
        const cam = GameManager.getInstance().currentLevel.camera;
        return CollisionUtil.getInstance().isInside(
            this.getX(), this.getY(), this.w, this.h, 
            cam.viewX, cam.viewY, cam.viewWidth, cam.viewHeight//level or camera rect
        )
    }
}