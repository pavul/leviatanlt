import { AudioManager } from "../lib/audio/AudioManager.js";
import { AnimationLoop } from "../lib/graphic/AnimationLoop.js";
import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
import { Point } from "../lib/graphic/Point.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { Timer } from "../lib/time/Timer.js";
import { CollisionUtil } from "../lib/util/CollisionUtil.js";
import { BaseSprite } from "./BaseSprite.js";
import { EnemyState } from "./EnemyState.js";



export class Missil extends BaseSprite implements Updatable
{
    readonly MISSILE_DOWN:number = 0;
    readonly MISSILE_UP:number = 1;
    

    missileType:number;
    insideTimer: Timer;
    exploded:boolean; //to know if missile as exploded and animation changed

    missilImgMeasure:ImageMeasures;
    explosionImgMeasure:ImageMeasures;


    //to put friction on Y movement of the missile
    frictionSpdDown:number=40;
    frictionSpdUp:number=40;


    constructor(image: HTMLImageElement, imgMeasures?:ImageMeasures)
    {
        super(image,imgMeasures);
        this.spdX=40;
        this.spdY=50;
        this.missileType = this.MISSILE_DOWN;

        this.insideTimer= new Timer();
        this.insideTimer.setCounter(50);
        this.exploded=false;

        //missil sprite is set when instantiated
        this.missilImgMeasure = imgMeasures;

        //make explosion animation faster
        this.animationStepLimit=4;
        this.animationLoop = AnimationLoop.STOPATEND;
    }

    update(delta: number, args?: any[]):void
    {
        
        // console.log("moving bullet")
        if(this.visible && this.enable && !this.exploded)
        {
            //go upward or downard
            switch( this.missileType )
            {
                case 0://down

                if( this.angle < 1.047)
                {
                    this.angle+= ( 4 * Math.PI) / 180 ;
                }
                this.move( this.spdX*delta, this.spdY*delta );
                break;

                case 1://up
                if( this.angle > -1.047)
                {
                    this.angle-= ( 4 * Math.PI) / 180 ;
                }
                this.move( this.spdX*delta, -this.spdY*delta );
                break;
            }

            //COLLISION TESTING
            //check for any enemy collision
            // if( this.getY() <= 50)
            // {
            //     const prevPos = new Point( this.getX()+this.w/2, this.getY()+this.h/2 );
            //     this.setNewAnimation(this.image, this.explosionImgMeasure)
            //     this.exploded=true;
            //     this.angle=0;
            //     this.currentFrame=0;
                
            //     this.setPosition( prevPos.x-this.w/2, prevPos.y-this.h/2);
            // }

            //check if out of view and disable/destroy
            //CHECK HERE FOR WHEN IS EXPLOSION
            this.insideTimer.process(()=>{
                //if outside the view, then disable it
                if( !this.isInsideView(this.getX(), this.getY(), this.w, this.h ))
                {
                    this.visible=false;
                    this.enable=false;
                }
                this.insideTimer.setCounter(30);
            });
            
        }
        else if( this.exploded && this.animationEnd )
        {//if missile impacted and exploded, check for animation end and destroy

            
            
            this.animationEnd=false;
            this.exploded=false;
            this.visible=false;
            this.enable=false;
            this.setNewAnimation(this.image, this.missilImgMeasure);
            this.insideTimer.setCounter(0);//disabling timer
            
            // console.log("missil exploded animation end")
            // console.log( JSON.stringify(this) )

            //don'tchange stepLimit cause is one frame
            //when missile is not exploded
            //this.animationStepLimit
        }

        
            //check for collisions here
            const enemies = args[0];

            
            for( const enemy of enemies)
            {
                if(enemy.label.includes("ene_bullet"))continue;
                
                if( enemy.visible && enemy.enable && enemy.state !== EnemyState.DESTROYED && enemy.state !== EnemyState.DISABLED )
                {

                    let isColliding = false;
                    if( this.exploded && this.currentFrame <= 5 )
                    {
                        isColliding = CollisionUtil.getInstance()
                            .spriteCircleColision( enemy, this.getX()+this.w/2, this.getY()+this.h/2, this.w/2 );
                    }
                    else
                    {
                        isColliding = CollisionUtil.getInstance().spriteRectangleCollision(this, enemy)
                    }

                        if( isColliding )
                        {
                        if( enemy.label.includes("boss") )
                        {
                            //deal damage to boss
                        }
                        else//check for other kinds of enemies
                        {
                            this.exploteMissil();
                        //laser wont dissapear but enemy will be destroyed
                        enemy.state = EnemyState.DESTROYED;
                        }


                    }
                }
            }
        

    }

    exploteMissil()
    {
        AudioManager.play("misexplsfx", false);//play only if shoot is not playing
        const prevPos = new Point( this.getX()+this.w/2, this.getY()+this.h/2 );
            this.setNewAnimation(this.image, this.explosionImgMeasure)
            this.exploded=true;
            this.angle=0;
            this.currentFrame=0;
            this.setPosition( prevPos.x-this.w/2, prevPos.y-this.h/2);
    }

}