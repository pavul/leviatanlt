


export enum EnemyType{ 
    ENEMY_1,
    ENEMY_2,
    ENEMY_3,
    ENEMY_4,
    TURRET_BLUE_UP,
    TURRET_BLUE_DOWN,
    TURRET_GREEN_UP,
    TURRET_GREEN_DOWN,
    BOSS,
    BULLET,
    SQR_BULLET,
    LASER,
    }