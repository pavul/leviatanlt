


export enum EnemyState{
    IDDLE,
    MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_UP,
    MOVE_DOWN,
    FOLLOW,//usable for when we want to follow an sprite, like en enemy follows the player
    DISABLED,//means we want to enable=false and visible=false, to put out an instance that went out of view
    DESTROYED,//means we need to destroy in the view changin the animation for some explotion or dead
    MOVE_ANY, //for object going in any direction like bullets towards an object

}