import { Camera } from "../lib/camera/Camera.js";
import { AnimationLoop } from "../lib/graphic/AnimationLoop.js";
import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
// import { GameManager } from "../lib/manager/GameManager.js";
// import { Point } from "../lib/graphic/Point.js";
import { Timer } from "../lib/time/Timer.js";
import { MathUtil } from "../lib/util/MathUtil.js";
import { SpriteUtil } from "../lib/util/SpriteUtil.js";
import { BaseSprite } from "./BaseSprite.js";
import { EnemyState } from "./EnemyState.js";
import { EnemyType } from "./EnemyType.js";
import { Ship } from "./Ship.js";
import { ImageData } from "../lib/ntfc/ImageData.js"
import { AudioManager } from "../lib/audio/AudioManager.js";
import { GameData } from "../lib/manager/GameData.js";


export class BaseEnemy extends BaseSprite {

    hp: number;
    state: EnemyState;//for FSM
    type: EnemyType; //enemy tipe if normal or boss
    spawnChance: number;
    score: number;//how many points this enemy gives

    static powerups: BaseSprite[] = [];
    static eneBullets: BaseEnemy[] = [];
    static eneSquaredBullets: BaseEnemy[] = [];
    static eneLasers: BaseEnemy[] = [];


    shootTimer: Timer;//to shoot at ship
    destroyTimer: Timer;//to destroy the ship if collided with 
    changeStateTimer: Timer;//to change the state of the sprite
    isInsideTimer: Timer;

    imgDatas = new Map<string, ImageData>();
    isDestroyed:boolean;

    bulletDmg:number;
    laserDmg:number;

    constructor(img: HTMLImageElement, imagMeasures: ImageMeasures, enemyType: EnemyType, imgDatas: Map<string, ImageData>) {
        super(img, imagMeasures);

        this.imgDatas = imgDatas;
        //depending of the label (enemy kind or name)
        //the variables will be set, for instance:
        //enemy1 will have all the same hp and score, sprite etc.
        this.type = enemyType;
        this.initEnemy();


        this.spawnChance = 20;
        this.isDestroyed=false;
        //this will put the pvot at the center of the sprite
        //this.pivot = new Point( this.w/2, this.h/2 );

        this.bulletDmg=5;
        this.laserDmg=10;
    }

    initEnemy() {
        this.state = EnemyState.IDDLE;

        //all instances will be checking if they are enabled and outside the view
        //if so they will be disabled.
        //if instance enabled=false, means is waiting to enter the game, otherwise means
        //entered the view and went out, so must be disabled/destroyed
        this.isInsideTimer = new Timer();
        this.isInsideTimer.setCounter(50);//needed to start process of isInsideTimer


        //enemy is visible but disabled
        //this has to be put randomly on the level
        //if enable = false, enemy wont move, is just like in stand by
        //until the view reaches the enemy so it can be moved
        this.enable = false;
        this.setPosition(-100, -100);

        //set timers for all instances, but for now only ene1 will set it
        this.shootTimer = new Timer();
        this.changeStateTimer = new Timer();

        switch (this.type) 
        {

            case EnemyType.ENEMY_1:
                this.hp = 1;
                this.score = 10;
                this.spdX = 50;
                this.spdY = 50;
                this.state = EnemyState.MOVE_LEFT;

                this.shootTimer.setCounter(MathUtil.getRandomRangeInt(50, 100));
                this.changeStateTimer.setCounter(MathUtil.getRandomRangeInt(50,70))

                this.setPosition(MathUtil.getRandomRangeInt(340, 2500), MathUtil.getRandomRangeInt(30, 150))
                break;
            case EnemyType.ENEMY_2:
                this.hp = 2;
                this.score = 20;
                this.spdX = 50;
                this.spdY = 50;
                this.state = EnemyState.MOVE_LEFT;

                this.shootTimer.setCounter(MathUtil.getRandomRangeInt(50, 80));
                this.changeStateTimer.setCounter(MathUtil.getRandomRangeInt(50,70))
                this.setPosition(MathUtil.getRandomRangeInt(340, 2500), MathUtil.getRandomRangeInt(30, 150))
                break;
            case EnemyType.ENEMY_3:
                this.hp = 3;
                this.score = 30;
                this.spdX = 60;
                this.spdY = 60;
                this.state = EnemyState.MOVE_LEFT;

                this.setPosition(MathUtil.getRandomRangeInt(340, 2500), MathUtil.getRandomRangeInt(30, 150))
                break;
            case EnemyType.ENEMY_4:
                this.hp = 4;
                this.score = 40;
                this.spdX = 70;
                this.spdY = 70;
                this.state = EnemyState.MOVE_LEFT;
                this.setPosition(MathUtil.getRandomRangeInt(340, 2500), MathUtil.getRandomRangeInt(30, 150))
                break;
            case EnemyType.TURRET_BLUE_DOWN:
                this.hp = 2;
                this.score = 20;
                break;
            case EnemyType.TURRET_BLUE_UP:
                this.hp = 2;
                this.score = 20;
                break;
            case EnemyType.TURRET_GREEN_DOWN:
                this.hp = 2;
                this.score = 20;
                break;
            case EnemyType.TURRET_GREEN_UP:
                this.hp = 2;
                this.score = 20;
                break;

            case EnemyType.BULLET:
            case EnemyType.SQR_BULLET:
                // console.log("GENERATING BULLET")
                this.hp = 0;
                this.score = 0;
                this.spdX = 60;
                this.spdY = 60;
                this.state = EnemyState.MOVE_ANY;
                break;
        }


    }

    // setPosition(x: number, y: number) {
    //     this.setX(x - this.w / 2);
    //     this.setY(y - this.h / 2);
    // }


    /**
     * this will be spawned after destroyed animation ends
     */
    spawnPowerUp() {

        if (Math.ceil(MathUtil.getRandomRangeInt(0, 100) / this.spawnChance) == 1) {
            // spawn powerup at center of this
            for(const id in BaseEnemy.powerups )
            {
                const pwr =  BaseEnemy.powerups[id];
                if( !pwr.visible )
                {
                    pwr.visible=true;
                    pwr.setPosition( this.getX(), this.getY() );
                    break;
                }
            } 

        }
    }

    /**
     * this method is used for the bosses to finish level
     * and load a new one, there must be a global variable for current level
     */
    completeLevel() {

    }


    update(delta: number, args?: any[]) 
    {
        const cam = args[1];
        //check here if inside view to put enable to true
        //check if enemy is inside view to enable or outside view to be disabled

        this.isOutsideView(cam);

        this.isWaiting(cam);


        if (!this.enable) return;

        const ship = <Ship>args[0];

        //move depending state
        switch (this.state) 
        {
            case EnemyState.MOVE_LEFT:
                this.moveX(-this.spdX * delta);
                break;
            case EnemyState.MOVE_UP:
                this.moveX(-this.spdX * delta);
                this.moveY(-this.spdY * delta);
                break;
            case EnemyState.MOVE_DOWN:
                this.moveX(-this.spdX * delta);
                this.moveY(this.spdY * delta);
                break;
            case EnemyState.MOVE_ANY:
                //this is used for bullets, a bullet can move anywhere 
                this.move(this.spdX * delta, this.spdY * delta);

                break;
            case EnemyState.DISABLED:
                // if any sprite is disabled will be moved directly out of the view
                //and will be invisible and enabled to false
                this.visible = false;
                this.enable = false;
                this.setPosition(-100, -100);
                this.animationEnd=false;
                break;
            case EnemyState.DESTROYED:
                

                //set score points
                // GameManager.getInstance().gameData.
                // console.log(`points gained: ${this.score}`);

                // change to explotion animation 
                if( this.isDestroyed === false )
                {
                    this.increaseScore( this.score );
                    this.isDestroyed=true;
                    this.setNewAnimation( this.image, this.imgDatas.get("eneexplosion2").imageMeasures )
                    this.animationLoop = AnimationLoop.STOPATEND;
                    this.animationStepLimit=8;
                    this.spdX=0;
                    this.spdY=0;
                    this.setPosition(this.getX()-6, this.getY()-6)
                    //if is destroyed means this enemy was hit by a bullet or ship
                    AudioManager.play("eneexplsfx", false);
                    
                    //stop all timers:
                    this.shootTimer.setCounter(0)
                    this.isInsideTimer.setCounter(0)

                    this.animationEnd=false;
                }
                

                if (this.animationEnd) 
                {
                    this.spawnPowerUp();
                    this.state = EnemyState.DISABLED;
                    //set enemy animation again
                    // switch (this.type) {
                    //     case EnemyType.ENEMY_1:
                    //         this.setNewAnimation(this.image, this.imgDatas.get("ene1").imageMeasures)
                    //         this.animationLoop = AnimationLoop.FORWARD;
                    //         break;
                    //     case EnemyType.ENEMY_2:
                    //         this.setNewAnimation(this.image, this.imgDatas.get("ene2").imageMeasures)
                    //         this.animationLoop = AnimationLoop.FORWARD;
                    //         break;
                    //     case EnemyType.ENEMY_3:
                    //         this.setNewAnimation(this.image, this.imgDatas.get("ene3").imageMeasures)
                    //         this.animationLoop = AnimationLoop.FORWARD;
                    //         break;
                    //     case EnemyType.ENEMY_4:
                    //         this.setNewAnimation(this.image, this.imgDatas.get("ene4").imageMeasures)
                    //         this.animationLoop = AnimationLoop.FORWARD;
                    //         break;
                    // }
                }

                break;

        }
        //this.move();

        //change state
        this.changeStateTimer.process(() => 
        {
         
            if(this.state === EnemyState.DESTROYED)return;
         
            switch (this.type) {
                case EnemyType.ENEMY_1:
                case EnemyType.ENEMY_2:
                case EnemyType.ENEMY_3:
                case EnemyType.ENEMY_4:

                    //for enemies 1 to 4 they will change their state randomly
                    const proval = Math.ceil(MathUtil.getRandomRangeInt(0, 100) / 33)
                    switch (proval) {
                        // case 1:
                        //     break;
                        case 2:
                            this.state = EnemyState.MOVE_UP;
                            break;
                        case 3:
                            this.state = EnemyState.MOVE_DOWN;
                            break;
                    }
                    break;
            }

        });

        //shoot At
        this.shootTimer.process(() => 
        {
            if(this.state === EnemyState.DESTROYED)return;
        
            let xx: number = 0;
            let yy: number = 0;

            if (ship !== undefined) {
                xx = ship.getX() + this.w / 2;
                yy = ship.getY() + this.h / 2;
                // console.log(`SHIP: ${xx}, ${yy}`)
            }

            switch (this.type) {
                case EnemyType.ENEMY_1:
                case EnemyType.ENEMY_2:
                case EnemyType.ENEMY_3:
                case EnemyType.ENEMY_4:

                    // console.log(`ENE bul length: ${BaseEnemy.eneBullets.length}`)
                    if (BaseEnemy.eneBullets.length)
                        for (const id in BaseEnemy.eneBullets) {
                            const b = BaseEnemy.eneBullets[id];
                            if (!b.visible && this.isInsideView(cam.viewX, cam.viewY, cam.viewWidth, cam.viewHeight)) {
                                this.isInsideTimer.setCounter(100);
                                b.visible = true; b.enable = true;
                                b.setPosition(this.getX()+4, this.getY()+8)
                                b.state = EnemyState.MOVE_ANY;
                                //no need to set delta in 100 cause that sped generated
                                //will be multiplied for delta on the state phase
                                SpriteUtil.moveTo<BaseEnemy>(b, xx, yy, 100);
                                break;
                            }
                        }
                    break;

                case EnemyType.TURRET_BLUE_DOWN:
                case EnemyType.TURRET_BLUE_UP:
                case EnemyType.TURRET_GREEN_DOWN:
                case EnemyType.TURRET_GREEN_UP:

                    if (BaseEnemy.eneSquaredBullets.length)
                        for (const id in BaseEnemy.eneSquaredBullets) {
                            const b = BaseEnemy.eneSquaredBullets[id];
                            if (!b.visible) {
                                b.visible = true;
                                b.enable = true;
                                b.setPosition(this.getX(), this.getY())
                                SpriteUtil.moveTo<BaseEnemy>(<BaseEnemy>b, xx, yy, 140);
                                // console.log(` MOVE TO: ${b.spdX}, ${b.spdY}`)
                                break;
                            }
                        }
                    break;

            }

            this.shootTimer.setCounter(MathUtil.getRandomRangeInt(50, 100));
        });




    }



    isWaiting(cam:Camera)
    {
        if( !this.enable && this.getX() < cam.viewX+cam.viewWidth+30 )
        {
            this.enable=true;
        }
    }

    /**
     * checks if the enemy is outside the view to disable it
     * but if is inside the view ,then it will enable it so it can be moved
     * @param cam 
     */
    isOutsideView(cam: Camera) {
        this.isInsideTimer.process(() => {

            if (this.enable && !this.isInsideView(cam.viewX, cam.viewY, cam.viewWidth, cam.viewHeight)) {
                //delete
                //change state to disabled
                this.state = EnemyState.DISABLED;
                //stop shooting
                this.shootTimer.setCounter(0);
            }
            else
                this.isInsideTimer.setCounter(50);
        });
    }

    increaseScore(score:number)
    {
    let scr:number = parseInt( GameData.getData("score") )
    scr+=score;
    GameData.setData("score", scr); 
    }

}