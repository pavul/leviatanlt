import { EndLevel } from "../levels/EndLevel.js";
import { GameOverLevel } from "../levels/GameOverLevel.js";
import { TitleLevel } from "../levels/TittleLevel.js";
import { AudioManager } from "../lib/audio/AudioManager.js";
import { Camera } from "../lib/camera/Camera.js";
import { AnimationLoop } from "../lib/graphic/AnimationLoop.js";
import { HUDSprite } from "../lib/graphic/HUDSprite.js";
import { ImageMeasures } from "../lib/graphic/ImageMeasures.js";
import { Point } from "../lib/graphic/Point.js";
import { Collider } from "../lib/graphic/shape/Collider.js";
import { Controller } from "../lib/input/Controller.js";
import { ControllerState } from "../lib/input/ControllerState.js";
import { GameData } from "../lib/manager/GameData.js";
import { GameManager } from "../lib/manager/GameManager.js";
import { GameState } from "../lib/manager/GameState.js";
import { Updatable } from "../lib/ntfc/Updatable.js";
import { Timer } from "../lib/time/Timer.js";
import { CollisionUtil } from "../lib/util/CollisionUtil.js";
import { BaseSprite } from "./BaseSprite.js";
import { Bullet } from "./Bullet.js";
import { EnemyState } from "./EnemyState.js";
import { Laser } from "./Laser.js";
import { Missil } from "./Missil.js";



export class Ship extends BaseSprite implements Updatable
{

    readonly DIR_UP:number=4;
    readonly DIR_DOWN:number=2;
    readonly DIR_RIGHT:number=1;
    readonly DIR_LEFT:number=3;
    readonly DIR_NONE:number=0;

    readonly MAX_SHIELD_HP:number=50;
    readonly MAX_DRONE_HP:number=50;

    //states for ship
    readonly STATE_NORMAL:number=1;
    readonly STATE_DESTROYED:number=2;
    readonly STATE_INCOMING:number=3;
    readonly STATE_STAGE_CLEAR:number=4;
    


    missilWait:boolean;
    shotWait:boolean;

    //1 right
    //2 down
    //3 left
    //4 up
    direction:number;

    //this is the same value of camera speed, to move along the camera
    camSpd:number;
    camera:Camera;

    baseSpd:number;

    //keep powerups
    powerups:boolean[]=[];
    curPow:number;

    //keep sprites inside ship
    //lasers x 5, bullets x 5, drone, shield, power
    bullets:Bullet[]=[];
    missils:Missil[]=[];
    lasers:Laser[]=[];

    drone:BaseSprite;
    shield:BaseSprite;
    flame:BaseSprite;


    dronePosHistory:Point[]=[];

    shieldHealth:number;
    droneHealth:number;

    //draws bottom power HUD bar
    powerBarHud:HUDSprite;//set this on level init

    // gamePaused:boolean;

    explotionImgMeasures:ImageMeasures;
    shipImgMeasures:ImageMeasures;

    state:number;

    // destroyed:boolean;
    incomingTimer:Timer;

    gpad:Controller;

    collider:Collider;
    


    constructor(image: HTMLImageElement, imgMeasures?:ImageMeasures)
    {
        super(image, imgMeasures);
        this.shipImgMeasures = imgMeasures;

        for( let idx=0; idx < 7; idx++)
        {
            this.powerups[idx]=false;
        }


        //to know what is the current power selected
        this.curPow=-1;

        //to prevent multiple shots at once
        this.missilWait = false;
        this.shotWait = false;

        // this.camSpd=4;
        this.direction = this.DIR_NONE;
        this.baseSpd = 60;

        //init position history fro drone
        for(let i=0; i<20; i++)
        {
            this.dronePosHistory[i] = new Point(0,0);
        }

        //
        this.shieldHealth = this.MAX_SHIELD_HP;
        this.droneHealth = this.MAX_DRONE_HP;

        // this.powerups[6]=true;
        // this.curPow=6;

        // this.gamePaused = false;
        // this.destroyed=false;

        this.state= this.STATE_NORMAL;

        this.incomingTimer = new Timer();

        this.gpad = new Controller(navigator.getGamepads()[0]);

        // this.collider=;
        this.colliders.set("col", new Collider( 2, 4, 19, 6, this) )
    }


    /**
    * this will display powerups bottom bar
    * and will display correct names even if highlighted
    * @param ctx 
    */
    drawPwrUps( ctx:CanvasRenderingContext2D )
    {

        const padding:number = 20;
        for( let i=0; i<this.powerups.length; i++ )
        {

            let frame:number = 0;
            if(this.powerups[i]) 
            {
              frame = (this.curPow === i)?1:0;
            }
            else
            {
              frame = (this.curPow === i)? ((i+1)*2)+1:(i+1)*2 
            //   console.log(`i: ${i} - frame: ${frame}`)
            } 

            this.powerBarHud.render(ctx, frame, 
            new Point(this.camera.viewX+( this.powerBarHud.w * i )+padding,
            this.camera.viewY+ this.camera.viewHeight - this.powerBarHud.h ) );
        }

        
    }//

    update(delta: number, args?:any[]): void {

        
        if( this.state === this.STATE_STAGE_CLEAR )
        {
            this.moveX(200 * delta);
            //them move all other stuff related to ship
            this.flame.setPosition(this.getX()-(this.flame.w-2),this.getY()+4);

            if(this.powerups[4])
            {
                this.drone.moveX(200 * delta)
            }

            if(this.powerups[5])
            {
                this.shield.moveX(200 * delta)
            }

        }
        else if( this.state === this.STATE_INCOMING )
        {

            this.moveX(150 * delta);
            // console.log(` ${this.getX()} - ${this.getY()}`)

            this.incomingTimer.process(()=>{
            this.state = this.STATE_NORMAL;    
            this.alpha=1;
            this.animationEnd=false;
            this.direction=this.DIR_NONE;
            this.flame.visible=true;
            this.flame.setAnimationFrames(1,2);
            
            })
        }
        else if( this.state === this.STATE_DESTROYED )
        {
            if(this.animationEnd)
            {
                // this.visible=false;
                // move the ship outside the view
                this.setPosition(this.camera.viewX-40, this.camera.viewY+this.camera.viewHeight/2 - this.w/2)
                
                //set something to put the ship inside again
                this.state = this.STATE_INCOMING;
                this.setNewAnimation(this.image, this.shipImgMeasures);
                this.animationLoop=AnimationLoop.NONE;
                
                this.currentFrame=1;
                this.incomingTimer.setCounter(25);
                this.state = this.STATE_INCOMING;
                this.alpha=0.6;
                this.decreaseLive();
            }
        }
        else
        {

        //getting gamepad update if is connected
        const controllerState =  this.gpad.poll( navigator.getGamepads() )

        //if for some reason the gamepad is disconnected, game will be paused
        if( controllerState === ControllerState.DISCONNECTED )
        {

            GameManager.getInstance().currentLevel.gameState = GameState.PAUSED;
        }


        this.getGamepadInput();

         const shipSpd:number = this.baseSpd * delta;
         //move the ship
         this.checkViewBounds(shipSpd);
        
        //them move all other stuff related to ship
        this.flame.setPosition(this.getX()-(this.flame.w-2),this.getY()+4);

        if( this.powerups[5] )
            this.shield.setPosition(this.getX()-13,this.getY()-3 )
        // this.drone.setPosition(this.getX()-10,this.getY()-10 )


        //update drone speed and move it following ship
       if( this.direction !== this.DIR_NONE)
       {
        const p:Point = this.dronePosHistory.pop();// 
            this.drone.spdX = p.x;
            this.drone.spdY = p.y;
        
       }

       if( this.drone.visible && this.powerups[4] )
           this.drone.move();


           //CHECK COLLISIONS WITH ENEMIES AND POWERICONS
           const colUtil = CollisionUtil.getInstance()
           const enemies = args[0];
           for(let i in enemies)
           {
                const enemy = enemies[i];

                if( enemy.visible && enemy.enable && 
                    enemy.state !== EnemyState.DESTROYED && enemy.state !== EnemyState.DISABLED &&
                    colUtil.spriteRectangleCollision( this.colliders.get("col") , enemy))
                {
                    if( this.powerups[5] )
                    {
                        AudioManager.play("shieldcolsfx", false);
                        this.shieldHealth-=10;
                        enemy.visible=false;
                        enemy.enabled=false;
                        
                        if(this.shieldHealth<=0)
                        {
                            this.shieldHealth=0;
                            this.shield.visible=false;
                            this.powerups[5]=false;
                        }
                    }
                    else
                        this.destroy();
                }
           }


           //collide with powerUps
           const powericons = args[1];
           for(let i in powericons)
           {
               const pwr = powericons[i];
               if( pwr.visible && colUtil.spriteRectangleCollision( this, pwr ) )
               {
                AudioManager.play("getpower", false);
                   // console.log("POWERUP COLLISION")
                   pwr.visible = false;
                   this.increasePower();//used when power up is taken
                   break;
               }
           }


        }

        
    }


    /**
     * events for gamepad inputs
     */
    
    getGamepadInput()
    {

        if( this.gpad.isButtonReleased( Controller.PS4_BTN_OPTIONS) )
        {
            AudioManager.play("pausesfx")
            if( GameManager.getInstance().currentLevel.gameState === GameState.PLAYING )
            {
                GameManager.getInstance().currentLevel.gameState = GameState.PAUSED;
            }
            else{
                GameManager.getInstance().currentLevel.gameState = GameState.PLAYING;
            }
        }

        if(this.gpad.isButtonReleased( Controller.PS4_BTN_SQUARE) )
        {this.getInput( "z","keyUp");}
        if(this.gpad.isButtonReleased( Controller.PS4_BTN_X) )
        {this.getInput( "x","keyUp");}

        if( this.gpad.isButtonReleased( Controller.PS4_DPAD_DOWN) || 
            this.gpad.isButtonReleased( Controller.PS4_DPAD_UP) ||
            this.gpad.isButtonReleased( Controller.PS4_DPAD_LEFT) ||
            this.gpad.isButtonReleased( Controller.PS4_DPAD_RIGTH) )
        {
            this.getInput("s", "keyUp")
        }


        if(this.gpad.isButtonPressed( Controller.PS4_DPAD_DOWN) )
        {this.getInput( "s","keyDown");}

        if(this.gpad.isButtonPressed( Controller.PS4_DPAD_UP) )
        {this.getInput( "w","keyDown");}

        if(this.gpad.isButtonPressed( Controller.PS4_DPAD_LEFT) )
        {this.getInput( "a","keyDown");}

        if(this.gpad.isButtonPressed( Controller.PS4_DPAD_RIGTH) )
        {this.getInput( "d","keyDown");}

    }

    /**
     * to move the ship depending the input of keyboard controller
     * this goes on BaseLevel keyUp, keyDown methods , etc. 
     */
    getInput(value:any, type:string): void {

        if(this.state === this.STATE_DESTROYED)return;

        switch( type )
        {
            case "keyDown":
                switch(value)
                {
                    case "a":
                    case "A":
                    case"ArrowLeft":
                        if( this.direction !== this.DIR_LEFT) this.direction = this.DIR_LEFT;
                    break;

                    case "d":
                    case "D":
                    case"ArrowRight":
                        if( this.direction !== this.DIR_RIGHT) this.direction = this.DIR_RIGHT;
                    break;

                    case "w":
                    case "W":
                    case"ArrowUp":
                        if( this.direction !== this.DIR_UP) this.direction = this.DIR_UP;
                        if( this.currentFrame !== 0)this.currentFrame--;
                    break;

                    case "s":
                    case "S":
                    case"ArrowDown":
                        if( this.direction !== this.DIR_DOWN) this.direction = this.DIR_DOWN;
                        if( this.currentFrame !== 2)this.currentFrame++;
                    break;
                }
            
            break;
            case "keyUp":

                switch(value)
                {
                    //to shoot
                    case " "://space btn
                    case "j":
                    case "J":
                    case "z":
                    case "Z":
                       this.shoot();
                       this.shootMissiles();
                       this.droneShoot();
                    break;

                    case "k":
                    case "K":
                    case "x":
                    case "X":
                    this.selectPower();
                    break;

                    case "a":
                    case "A":
                    case "d":
                    case "D":
                    case "s":
                    case "S":
                    case "w":
                    case "W":
                    case"ArrowLeft":
                    case"ArrowRight":
                    case"ArrowUp":
                    case"ArrowDown":
                        this.direction = this.DIR_NONE;
                        if( this.currentFrame !== 1)this.currentFrame = 1;

                        this.drone.spdX = 0;
                        this.drone.spdY = 0;
                        break;
                    
                }
            
            break;
            case "btnPressed":
            
            break;
            case "btnReleased":
            
            break;
        }

       
        

    }

    /**
     * shoot bullets or lasers
     */
    shoot()
    {
        //laser if enabled
        if( this.powerups[3] )
        {
            
            //shoot laser
            for(let idx=0; idx< this.lasers.length; idx++)
            {
                const l = this.lasers[idx]
                if(!l.visible)
                {   
                    AudioManager.play("lasersfx", false);//play only if shoot is not playing
                    l.setPosition(this.getX()+this.w-6,this.getY()+6 )
                    l.visible = true;
                    l.enable = true;
                    break;
                }
            }
        }
        else //if not laser, then bullets
        {
            
            //shoot one bullet
            for(let idx=0; idx< this.bullets.length; idx++)
            {
                const b = this.bullets[idx]
                if(!b.visible)
                {   
                    AudioManager.play("shootsfx", false);
                    b.setPosition(this.getX()+this.w-6,this.getY()+6 )
                    b.visible=true;
                    b.enable=true;
                    b.spdX=b.hspd;
                    b.spdY=0;
                    b.insideTimer.setCounter(50);
                    break;
                }
            }

            //shoot up & down bullets
            if(this.powerups[2])
            {
                const arr = [];
                //shoot bullets
                for( let idx:number = 0; idx < this.bullets.length; idx++ )
                {
                    if( !this.bullets[idx].visible )
                    {
                        arr.push(this.bullets[idx]);
                        if(arr.length===2)break;
                    }
                }
    
                
                //0 upRight
                if( arr[0] !== undefined )
                {
                    AudioManager.play("shootsfx", false);
                    arr[0].setPosition(this.getX()+this.w-9,this.getY()+6 )
                    arr[0].spdY=-arr[0].vspd;
                    arr[0].visible=true;
                    arr[0].enable=true;
                    arr[0].insideTimer.setCounter(50);  
                }
                //1 downRight
                if( arr[1] !== undefined )
                {
                    arr[1].setPosition(this.getX()+this.w-9,this.getY()+6 )
                    arr[1].spdY=arr[0].vspd;
                    arr[1].visible=true;
                    arr[1].enable=true;
                    arr[1].insideTimer.setCounter(50);
                }
            }

        }

        
    }

    shootMissiles()
    {
        if( this.powerups[1] )
        {
            if( !this.missils[0].visible )
            {
                this.missils[0].angle=0;
                this.missils[0].setPosition(this.getX()+this.w/2,this.getY()+12);
                this.missils[0].visible = true;
                this.missils[0].enable = true;
                this.missils[0].insideTimer.setCounter(50);
                this.missils[0].exploded=false;
            } 

            if( !this.missils[1].visible )
            {
                this.missils[1].angle=0;
                this.missils[1].setPosition(this.getX()+this.w/2,this.getY());
                this.missils[1].visible = true;
                this.missils[1].enable = true;
                this.missils[1].insideTimer.setCounter(50);
                this.missils[1].exploded=false;
            } 
        }
    }


    droneShoot()
    {
        //won't shoot this if drone powerup is disabled
        if(!this.powerups[4])return;

        if( this.powerups[3] ){//laser
                for( let idx in this.lasers )
                {
                    if( !this.lasers[idx].visible && !this.lasers[idx].enable )
                    {
                        this.lasers[idx].visible=true;
                        this.lasers[idx].enable=true;
                        this.lasers[idx].setPosition( this.drone.getX()+8, this.drone.getY()+4 )
                        break;
                    }
                }
        }
        else //if( this.powerups[2] )//bullets
        {//there will be always bullets if drone is enabled
            for( let idx in this.bullets )
            {
                if( !this.bullets[idx].visible && !this.bullets[idx].enable )
                {
                    this.bullets[idx].visible=true;
                    this.bullets[idx].enable=true;
                    this.bullets[idx].spdX = this.bullets[idx].hspd;
                    this.bullets[idx].spdY = 0;
                    this.bullets[idx].setPosition( this.drone.getX()+10, this.drone.getY()+6 )
                    this.bullets[idx].insideTimer.setCounter(50);
                    break;
                }
            }
        }
    }

    selectPower()
    {
        //only select powers where powerup is false
        if(this.powerups[ this.curPow ])return;

        //
        this.powerups[ this.curPow ] = true;

        //play power selected YAYYY!
        AudioManager.play("selectpower", false)

        switch(this.curPow)
        {
            case 0:
                this.baseSpd = 80;
                this.flame.setAnimationFrames(3,4);//changes to red flame
            break;
            case 1://missiles

            break;
            case 2://bullets
                this.powerups[3]=false;//disable lasser
            break;
            case 3:
                this.powerups[2]=false;//disable bullet
            break;
            case 4://drone
                //set drone position at ship
                for(let i=0; i<20; i++)
                {
                    this.dronePosHistory[i] = new Point(0,0);
                }
                this.drone.setPosition( (this.getX()+this.w/2) - this.drone.w/2, (this.getY()+this.h/2) - this.drone.h/2 )
                this.drone.visible=true;
                this.drone.enable=true;
            break;
            case 5://shield
                //set shield hp
                this.shieldHealth=this.MAX_SHIELD_HP;
            break;
            case 6://power
                this.releasePower();
            break;
        }

        //make select soud here...
        this.curPow=-1;

    }

    render(ctx: CanvasRenderingContext2D): void {
        super.render(ctx);

        for(let b of this.bullets)b.render(ctx);
        for(let l of this.lasers)l.render(ctx);
        for(let m of this.missils)m.render(ctx);

        if(this.powerups[4])
           this.drone.render(ctx)

        if(this.powerups[5])
           this.shield.render(ctx)
        
        this.flame.render(ctx)


        //draw black bar at the bottom
        ctx.fillStyle = "#000";
        ctx.fillRect(this.camera.viewX, this.camera.viewY + (this.camera.viewHeight-11), this.camera.viewWidth, this.camera.viewHeight);
        //draw here the power ups obtained
        this.drawPwrUps(ctx);
    }

    increasePower()
    {
        this.curPow++;
        if( this.curPow > 6)this.curPow=0;
    }


    /**
     * this will make a white flash in the view and will destroy all enemies and bombs
     * that are inside the view
     * will set all enemies destroy so they can explode and bullets will dissapear
     */
    releasePower()
    {
        if(this.curPow===6)
        {
            console.log("release power")
            this.powerups[6] = false;

            //make sound

            //make flash
            this.camera.flashEffect.activate(true);
        }

    }


    /**
     * will check the ship if is out of view bounds and
     * won't let go beyond 
     */
    checkViewBounds(spd:number)
    {
        if( this.direction === this.DIR_DOWN )
        {
            if( this.getY()+this.h < this.camera.viewY+this.camera.viewHeight-11 )
            { 
                this.moveY(spd)
                this.dronePosHistory.unshift( new Point(0, spd ) )
            }
            else{
               // this.moveY(0)
                this.dronePosHistory.unshift(new Point(0, 0 ) )
            }
    
        }

        if( this.direction === this.DIR_UP )
        {
            if( this.getY() > this.camera.viewY + 2 )
            {
                this.moveY(-spd)
                this.dronePosHistory.unshift(new Point(0, -spd ) )
            }
            else{
                // this.moveY(0)
                 this.dronePosHistory.unshift(new Point(0, 0 ) )
             }
        }
        

        if( this.direction === this.DIR_RIGHT )
        {
            if( this.getX() + this.w < this.camera.viewX + this.camera.viewWidth )
            {
                this.moveX(spd)
                this.dronePosHistory.unshift(new Point(spd,0 ) )
            }else{
                // this.moveY(0)
                 this.dronePosHistory.unshift(new Point(0, 0 ) )
             }
        }

        if( this.direction === this.DIR_LEFT )
        {
            if( this.getX() > this.camera.viewX+2)
            {
                this.moveX(-spd)
                this.dronePosHistory.unshift(new Point(-spd,0 ) )
            }else{
                // this.moveY(0)
                 this.dronePosHistory.unshift(new Point(0, 0 ) )
             }
        }

    }
                
    /**
     * this destroys the ship and makes an explotion, it will
     * be respawned after a while
     */
        destroy(): void 
        {
                    AudioManager.play("shipexpsfx", false)
                    // destroy ship
                    // this.destroyed=true;
                    this.state = this.STATE_DESTROYED;
                    this.setNewAnimation(this.image, this.explotionImgMeasures);
                    this.animationLoop = AnimationLoop.STOPATEND;
                    this.spdX=0;
                    this.spdY=0;

                    //play destroy explotion sound

                    //what to do if drone exists?
                    this.flame.visible=false;



                    if(this.shield.visible)
                    {
                        this.shield.visible=false;
                        this.shield.enable=false;
                        
                    }

                    if(this.drone.visible)
                    {
                        this.drone.visible=false;
                        this.drone.enable=false;
                    }

                    //if ship destroyed, then we have to disable all powerups
                    for( const i in this.powerups)
                    {
                        this.powerups[i]=false;
                    }
                    this.curPow=0;
        }


        stageClear()
        {
            AudioManager.play("missionComplete", false);
            this.state = this.STATE_STAGE_CLEAR;
            setTimeout( ()=>{
                console.log("go to next level...")
                GameManager.getInstance().loadLevel( new EndLevel() );
            }, 6000)
        }

        /**
         * use when ship is destroyed and have to respawn
         */
       decreaseLive()
       {

        let lives =  parseInt(GameData.getData("lives"));
        lives-=1;
        GameData.setData("lives", lives);

        if(lives <= 0 )
        {
            this.state = -1;//no state if there is no lives remaining
            this.incomingTimer.setCounter(0);

            AudioManager.stopAll();


            //check first continues
            const continues = GameData.getData("continues", true);
            if(continues <=0 )
                setTimeout( ()=>{GameManager.getInstance().loadLevel( new TitleLevel() )}, 3000);
            else
                setTimeout( ()=>{GameManager.getInstance().loadLevel( new GameOverLevel() )}, 3000);
        }

       }


}